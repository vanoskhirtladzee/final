package com.example.afinal

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object RetrofitClient {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://www.balldontlie.io/api/v1/")
        .build()

    private var service = retrofit.create(ApiRetrofit::class.java)

    fun getRequest(path: String, callbackHandler: CallbackHandler) {
        val call = service.getRequest(path)

        call.enqueue(callback(callbackHandler))
    }

    private fun callback(callbackHandler: CallbackHandler) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callbackHandler.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            callbackHandler.onSuccess(response.body().toString())
        }
    }
}

interface ApiRetrofit {
    @GET("{path}")
    fun getRequest(@Path("path") path: String): Call<String>
}