package com.example.afinal

interface CallbackHandler {
    fun onFailure(message: String) {}
    fun onSuccess(response: String) {}
}