package com.example.afinal

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.team_recyclerview_layout.view.*

class TeamRecyclerViewAdapter(
    private val teams: MutableList<TeamModel.Data>,
    private val mainActivity: MainActivity
) : RecyclerView.Adapter<TeamRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var team: TeamModel.Data

        fun onBind() {
            team = teams[adapterPosition]

            itemView.abbreviationTextView.text = team.abbreviation
            itemView.cityTextView.text = team.city
            itemView.conferenceTextView.text = team.conference
            itemView.divisionTextView.text = team.division
            itemView.fullNameTextView.text = team.fullName
            itemView.nameTextView.text = team.name


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.team_recyclerview_layout, parent, false)
        )
    }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()


    }
}