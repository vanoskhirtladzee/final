package com.example.afinal

import com.google.gson.annotations.SerializedName

class TeamModel {
    var data = ArrayList<Data>()

    class Data {
        var id = 0
        var abbreviation = ""
        var city = ""
        var conference = ""
        var division = ""
        @SerializedName("full_name")
        var fullName = ""
        var name = ""
    }
}