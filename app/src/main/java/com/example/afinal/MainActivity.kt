package com.example.afinal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var teams = mutableListOf<TeamModel.Data>()
    private lateinit var adapter: TeamRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        RetrofitClient.getRequest("teams", object : CallbackHandler {
            override fun onSuccess(response: String) {
                val team = Gson().fromJson(response, TeamModel::class.java)

                teams.addAll(team.data)

                teamRecyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = TeamRecyclerViewAdapter(teams, this@MainActivity)
                teamRecyclerView.adapter = adapter
            }
        })
    }
}
